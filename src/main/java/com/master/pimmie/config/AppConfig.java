package com.master.pimmie.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.math.BigDecimal;

@EnableAutoConfiguration
public class AppConfig {
    public static final BigDecimal initX = new BigDecimal(100.0);
    public static final BigDecimal initY = new BigDecimal(200.0);
    public static final BigDecimal offsetX = new BigDecimal(20.0);
    public static final BigDecimal offsetY = new BigDecimal(-10.0);
    public static final BigDecimal distanceX = new BigDecimal(200.0);
    public static final BigDecimal distanceY = new BigDecimal(150.0);
    public static final BigDecimal distanceTY = new BigDecimal(75.0);
}
