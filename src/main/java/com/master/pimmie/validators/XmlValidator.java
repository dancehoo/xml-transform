package com.master.pimmie.validators;

import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class XmlValidator {

    public String validateXMLFile(String xmlString, String xsdFile){
        String validation = null;
        try{
            InputStream xmlSource = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
            Source xmlFile = new StreamSource(xmlSource);
            InputStream xsdSource = getClass().getResourceAsStream("/" +xsdFile);
            Source schemaSource = new StreamSource(xsdSource);
            SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = schemaFactory.newSchema(schemaSource);
            Validator validator = schema.newValidator();
            try {
                validator.validate(xmlFile);
            } catch (SAXException ex) {
                validation = ex.getMessage();
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return validation;
    }
}
