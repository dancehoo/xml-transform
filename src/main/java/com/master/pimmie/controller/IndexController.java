package com.master.pimmie.controller;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.master.pimmie.model.input.Queue;
import com.master.pimmie.model.input.Service;
import com.master.pimmie.model.input.WaitingLine;
import com.master.pimmie.model.output.*;
import com.master.pimmie.validators.XmlValidator;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.master.pimmie.config.AppConfig.*;

@Controller
public class IndexController {

    private ResourceLoader resourceLoader;

    @GetMapping("/index")
    public String index(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "index";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file,
                         RedirectAttributes redirectAttributes, HttpServletResponse response) throws IOException {
        if (! validatePass(file, redirectAttributes)) return "redirect:index";
        BufferedReader br = null;
        InputStreamReader inputStreamReader = null;
        ServletOutputStream outStream = null;
        try {
            StringBuilder xmlString= new StringBuilder();
            inputStreamReader = new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8);
            br = new BufferedReader(inputStreamReader);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                xmlString.append(sCurrentLine.trim());
            }
            String error = new XmlValidator().validateXMLFile(xmlString.toString(), "Input.xsd");
            if(error!=null){
                redirectAttributes.addFlashAttribute("message",
                        "XML file error '" + error + "'");
                return "redirect:index";
            }
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
            Queue queue = xmlMapper.readValue(xmlString.toString(), Queue.class);
            Pnml pnml = genOutput(queue);
            response.setContentType("application/xml");
            response.setHeader("Content-Disposition", "attachment;filename=Output_SPN.xml");
            String xmlSPNString = xmlMapper.writeValueAsString(pnml);
            outStream = response.getOutputStream();
            outStream.println(xmlSPNString);
            outStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) br.close();
            if (inputStreamReader != null) inputStreamReader.close();
            if (outStream != null) outStream.close();
        }
        return null;
    }

    private boolean validatePass(MultipartFile file, RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return false;
        } else if (!Objects.equals(file.getContentType(), "text/xml")) {
            redirectAttributes.addFlashAttribute("message", "Please select a xml to upload");
            return false;
        }
        return true;
    }

    private Pnml genOutput(Queue queue) {
        Pnml output = new Pnml();
        Net net = new Net();
        net.setId("Net-One");
        net.setType("P/T net");
        Token token = new Token();
        token.setBlue(0);
        token.setEnabled(true);
        token.setId("Default");
        token.setGreen(0);
        token.setRed(0);
        net.setToken(token);
        List<Place> places = new ArrayList<>();
        BigDecimal xP = initX;
        BigDecimal yP = initY;
        long currentSeq = 0;
        for (WaitingLine waitingLine : queue.getWaitingLines()) {
            Place place = new Place();

            Capacity capacity = new Capacity();
            capacity.setValue(0);

            GraphicsP graphicsP = new GraphicsP();
            Position position = new Position();

            if (currentSeq != 0 && currentSeq == waitingLine.getSeq()) {
                yP = yP.add(distanceY);
            } else {
                xP = xP.add(distanceX);
                yP = initY;
            }
            position.setX(xP);
            position.setY(yP);
            graphicsP.setPosition(position);

            InitialMarking initialMarking = new InitialMarking();
            initialMarking.setValue("Default,0");
            GraphicsO graphicsO = new GraphicsO();
            Offset offset = new Offset();
            offset.setX(BigDecimal.ZERO);
            offset.setY(BigDecimal.ZERO);
            graphicsO.setOffset(offset);
            initialMarking.setGraphics(graphicsO);

            Name name = new Name();
            name.setValue(waitingLine.getId());
            GraphicsO nameGraphicsO = new GraphicsO();
            Offset nameOffset = new Offset();
            nameOffset.setY(offsetY);
            nameOffset.setX(offsetX);
            nameGraphicsO.setOffset(nameOffset);
            name.setGraphics(nameGraphicsO);

            place.setId(waitingLine.getId());
            place.setGraphics(graphicsP);
            place.setCapacity(capacity);
            place.setInitialMarking(initialMarking);
            place.setName(name);
            places.add(place);

            currentSeq = waitingLine.getSeq();
        }
        List<Transition> transitions = new ArrayList<>();
        List<Arc> arcs = new ArrayList<>();
        Map<String, BigDecimal> leftMap = new HashMap();
        for (Service service : queue.getServices()) {
            Transition transition = new Transition();

            GraphicsP graphicsP = new GraphicsP();
            Position position = new Position();
            BigDecimal currentY = BigDecimal.ZERO;
            for (Place place : places) {
                if (place.getId().equals(service.getLeft().get(0))) {
                    position.setX(place.getGraphics().getPosition().getX().add(distanceX.divide(new BigDecimal(2), RoundingMode.DOWN)));
                    currentY = place.getGraphics().getPosition().getY();
                    break;
                }
            }
            if (leftMap.containsKey(service.getLeft().get(0))) {
                currentY = leftMap.get(service.getLeft().get(0)).add(distanceTY);
                position.setY(currentY);
                leftMap.put(service.getLeft().get(0), currentY);
            } else {
                position.setY(currentY);
                leftMap.put(service.getLeft().get(0), currentY);
            }
            graphicsP.setPosition(position);

            InfiniteServer infiniteServer = new InfiniteServer();
            infiniteServer.setValue(false);

            Orientation orientation = new Orientation();
            orientation.setValue(BigDecimal.ZERO);

            Priority priority = new Priority();
            priority.setValue(BigDecimal.ONE);

            Rate rate = new Rate();
            rate.setValue(BigDecimal.ONE);

            Timed timed = new Timed();
            timed.setValue(true);

            Name name = new Name();
            GraphicsO graphicsO = new GraphicsO();
            Offset offset = new Offset();
            offset.setX(new BigDecimal(20.714285714285708));
            offset.setY(new BigDecimal(-7.857142857142853));
            graphicsO.setOffset(offset);
            name.setGraphics(graphicsO);
            name.setValue(service.getId());

            transition.setId(service.getId());
            transition.setGraphics(graphicsP);
            transition.setInfiniteServer(infiniteServer);
            transition.setName(name);
            transition.setOrientation(orientation);
            transition.setPriority(priority);
            transition.setRate(rate);
            transition.setTimed(timed);
            transitions.add(transition);

            Arc arc;
            for (String left: service.getLeft()) {
                arc = new Arc();
                arc.setId(left+" to "+service.getId());
                arc.setSource(left);
                arc.setTarget(service.getId());
                arc.setGraphics(new GraphicsO());
                Inscription inscription = new Inscription();
                inscription.setGraphics(new GraphicsP());
                inscription.setValue("Default,1");
                arc.setInscription(inscription);
                Tagged tagged = new Tagged();
                tagged.setValue(false);
                Type type = new Type();
                type.setValue("normal");
                arc.setTagged(tagged);
                arc.setType(type);
                arcs.add(arc);
            }

            for (String right: service.getRight()) {
                arc = new Arc();
                arc.setId(service.getId()+" to "+right);
                arc.setSource(service.getId());
                arc.setTarget(right);
                arc.setGraphics(new GraphicsO());
                Inscription inscription = new Inscription();
                inscription.setGraphics(new GraphicsP());
                inscription.setValue("Default,1");
                arc.setInscription(inscription);
                Tagged tagged = new Tagged();
                tagged.setValue(false);
                Type type = new Type();
                type.setValue("normal");
                arc.setTagged(tagged);
                arc.setType(type);
                arcs.add(arc);
            }
        }
        net.setPlace(places);
        net.setTransition(transitions);
        net.setArc(arcs);
        output.setNet(net);
        return output;
    }
}
