package com.master.pimmie.model.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Token {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    @JacksonXmlProperty(isAttribute = true)
    private boolean enabled;
    @JacksonXmlProperty(isAttribute = true)
    private int red;
    @JacksonXmlProperty(isAttribute = true)
    private int green;
    @JacksonXmlProperty(isAttribute = true)
    private int blue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
