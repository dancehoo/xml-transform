package com.master.pimmie.model.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Net {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    @JacksonXmlProperty(isAttribute = true)
    private String type;
    private Token token;
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Place> place;
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Transition> transition;
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Arc> arc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public List<Place> getPlace() {
        return place;
    }

    public void setPlace(List<Place> place) {
        this.place = place;
    }

    public List<Transition> getTransition() {
        return transition;
    }

    public void setTransition(List<Transition> transition) {
        this.transition = transition;
    }

    public List<Arc> getArc() {
        return arc;
    }

    public void setArc(List<Arc> arc) {
        this.arc = arc;
    }
}
