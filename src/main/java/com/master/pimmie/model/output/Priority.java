package com.master.pimmie.model.output;

import java.math.BigDecimal;

public class Priority {
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
