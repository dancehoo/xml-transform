package com.master.pimmie.model.output;

public class InfiniteServer {
    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    private boolean value;
}
