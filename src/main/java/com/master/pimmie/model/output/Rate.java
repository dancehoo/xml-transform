package com.master.pimmie.model.output;

import java.math.BigDecimal;

public class Rate {
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
