package com.master.pimmie.model.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Transition {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    private GraphicsP graphics;
    private Name name;
    private Orientation orientation;
    private Rate rate;
    private Timed timed;
    private InfiniteServer infiniteServer;
    private Priority priority;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GraphicsP getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsP graphics) {
        this.graphics = graphics;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public Timed getTimed() {
        return timed;
    }

    public void setTimed(Timed timed) {
        this.timed = timed;
    }

    public InfiniteServer getInfiniteServer() {
        return infiniteServer;
    }

    public void setInfiniteServer(InfiniteServer infiniteServer) {
        this.infiniteServer = infiniteServer;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
}
