package com.master.pimmie.model.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;

public class Offset {
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal x;
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal y;

    public BigDecimal getX() {
        return x;
    }

    public void setX(BigDecimal x) {
        this.x = x;
    }

    public BigDecimal getY() {
        return y;
    }

    public void setY(BigDecimal y) {
        this.y = y;
    }
}
