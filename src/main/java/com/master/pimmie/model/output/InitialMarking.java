package com.master.pimmie.model.output;

public class InitialMarking {
    private String value;
    private GraphicsO graphics;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public GraphicsO getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsO graphics) {
        this.graphics = graphics;
    }
}
