package com.master.pimmie.model.output;

public class Inscription {
    private String value;
    private GraphicsP graphics;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public GraphicsP getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsP graphics) {
        this.graphics = graphics;
    }
}
