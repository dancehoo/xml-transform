package com.master.pimmie.model.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Place {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    private GraphicsP graphics;
    private Name name;
    private InitialMarking initialMarking;
    private Capacity capacity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GraphicsP getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsP graphics) {
        this.graphics = graphics;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public InitialMarking getInitialMarking() {
        return initialMarking;
    }

    public void setInitialMarking(InitialMarking initialMarking) {
        this.initialMarking = initialMarking;
    }

    public Capacity getCapacity() {
        return capacity;
    }

    public void setCapacity(Capacity capacity) {
        this.capacity = capacity;
    }
}
