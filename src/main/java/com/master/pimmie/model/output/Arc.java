package com.master.pimmie.model.output;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Arc {
    @JacksonXmlProperty(isAttribute = true)
    private String id;
    @JacksonXmlProperty(isAttribute = true)
    private String source;
    @JacksonXmlProperty(isAttribute = true)
    private String target;
    private GraphicsO graphics;
    private Inscription inscription;
    private Tagged tagged;
    private Type type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public GraphicsO getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsO graphics) {
        this.graphics = graphics;
    }

    public Inscription getInscription() {
        return inscription;
    }

    public void setInscription(Inscription inscription) {
        this.inscription = inscription;
    }

    public Tagged getTagged() {
        return tagged;
    }

    public void setTagged(Tagged tagged) {
        this.tagged = tagged;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
