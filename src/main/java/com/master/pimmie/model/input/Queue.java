package com.master.pimmie.model.input;

import java.util.List;

public class Queue {
    private List<WaitingLine> waitingLines;
    private List<Service> services;

    public List<WaitingLine> getWaitingLines() {
        return waitingLines;
    }

    public void setWaitingLines(List<WaitingLine> waitingLines) {
        this.waitingLines = waitingLines;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
