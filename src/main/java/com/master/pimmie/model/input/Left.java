package com.master.pimmie.model.input;

public class Left {
    private String left;

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }
}
