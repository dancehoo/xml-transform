package com.master.pimmie.model.input;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.List;

public class Service {
    private String id;
    @JacksonXmlElementWrapper(localName = "lefts")
    private List<String> left;
    @JacksonXmlElementWrapper(localName = "rights")
    private List<String> right;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getLeft() {
        return left;
    }

    public void setLeft(List<String> left) {
        this.left = left;
    }

    public List<String> getRight() {
        return right;
    }

    public void setRight(List<String> right) {
        this.right = right;
    }
}
