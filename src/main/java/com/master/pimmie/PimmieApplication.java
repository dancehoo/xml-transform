package com.master.pimmie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PimmieApplication {

    public static void main(String[] args) {
        SpringApplication.run(PimmieApplication.class, args);
    }

}
